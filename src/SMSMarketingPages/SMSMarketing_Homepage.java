package SMSMarketingPages;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SMSMarketing_Homepage {

	
	public WebDriver driver;
	
	By Overview = By.xpath("(//*[@href = '/sms/overview'])[2]");
	By Campaigns = By.xpath("(//*[@href = '/sms/campaign'])[3]");
	By SuppressionList = By.xpath("(//*[@href = '/sms/suppression-list'])[2]");
	
	
	By Campaigns_BroadCast = By.xpath("(//*[@href = '/sms/campaign'])[4]");
	By Campaigns_BroadCast_CreateCampaign = By.xpath("(//*[@href = '/sms/campaign-type'])[2]");
	By Campaigns_BroadCast_Delete = By.xpath("(//*[@title = 'Delete'])[1]");
	By Campaigns_BroadCast_Clone = By.xpath("(//*[@title = 'Clone'])[1]");
	By Campaigns_BroadCast_ConditionArrow = By.xpath("(//*[@class = 'open-close-caret'])[1]");
	By Campaigns_BroadCast_Export = By.xpath("(//*[@href = '/sms/campaign/nv/export/Y'])");
	
	By Campaigns_OnetoOne = By.xpath("(//*[@href = '/sms/one-to-one/compose'])[1]");	
	By Campaigns_OnetoOne_Gateway = By.xpath("//*[@name = 'smsGatewayId']");
	By Campaigns_OnetoOne_To = By.xpath("//*[@name = 'mobile']");
	By Campaigns_OnetoOne_Message = By.xpath("//*[@name = 'message']");
	By Campaigns_OnetoOne_Send = By.xpath("//*[@name = 'send']");
	
	
	By Campaigns_Recurring = By.xpath("(//*[@href = '/sms/recurring/campaign'])[1]");
	By Campaigns_Recurring_CreateCampaign = By.xpath("(//*[@href = '/sms/campaign-type/nv/re/100'])[2]");
	By Campaigns_Recurring_Delete = By.xpath("(//*[@title = 'Trash'])[2]");
	By Campaigns_Recurring_Names = By.xpath("(//*[@title = 'view design'])");  // Provide index to access a particular campaign
	
	
	By SuppressionList_DND = By.xpath("(//*[@href = '/sms/suppression-list'])[3]");
	By SuppressionList_DND_AddDNDNumbers = By.xpath("(//*[text() = 'Add new DND numbers'])[2]");
	By SuppressionList_DND_Export = By.xpath("(//*[@href = '/sms/suppression-list/nv/export/Y'])");
	
	By SuppressionList_BlackList = By.xpath("(//*[@href = '/sms/suppression-list/blacklist'])[1]");
	By SuppressionList_BlackList_AddBlacklistNumbers = By.xpath("(//*[text() = 'Add New Blacklist Numbers'])[2]");
	By SuppressionList_BlackList_Export = By.xpath("(//*[@href = '/sms/suppression-list/blacklist/nv/export/Y'])");
	
	By SuppressionList_InvalidSubscriber = By.xpath("(//*[@href = '/sms/suppression-list/invalid'])[1]");
	By SuppressionList_InvalidSubscriber_AddInvalidNumbers = By.xpath("(//*[text() = 'Add New Invalid Numbers'])[2]");
	By SuppressionList_InvalidSubscriber_Export = By.xpath("(//*[@href = '/sms/suppression-list/invalid/nv/export/Y'])");
		
	
	
	public SMSMarketing_Homepage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	
	
	public void OverviewClick()
	
	{
		driver.findElement(Overview).click();
	}
	
	public void CampaignsClick()
	{
		driver.findElement(Campaigns).click();
	}
	
	public void SuppressionListClick()
	{
		driver.findElement(SuppressionList).click();
	}
	
	public void Campaigns_BroadCastClick()
	{
		driver.findElement(Campaigns_BroadCast).click();
	}
	
	public void Campaigns_BroadCast_CreateCampaignClick()
	{
		driver.findElement(Campaigns_BroadCast_CreateCampaign).click();
	}
	
	public void Campaigns_BroadCast_DeleteClick() throws Exception
	{
		driver.findElement(Campaigns_BroadCast_Delete).click();
		
		driver.findElement(By.xpath("(//*[@id = 'capsVal'])")).sendKeys("DELETE");
		Thread.sleep(1500);
		driver.findElement(By.xpath("(//*[text() = 'Delete'])[2]")).click();
	}
	
	public void Campaigns_BroadCast_CloneClick()
	{
		driver.findElement(Campaigns_BroadCast_Clone).click();
	String nameofbutton = driver.findElement(By.xpath("(//*[@name = 'clone'])")).getText();
	System.out.println("Name of button on clone confirmation prompt = "+nameofbutton);
	driver.findElement(By.xpath("(//*[@name = 'clone'])")).click();
	
	}
	
	public void Campaigns_BroadCast_ConditionArrowClick() throws Exception
	{
		driver.findElement(Campaigns_BroadCast_ConditionArrow).click();
		Thread.sleep(1500);
		driver.findElement(Campaigns_BroadCast_ConditionArrow).click();
	}
	

	
	public void Campaigns_BroadCast_ExportClick() 
	{
		driver.findElement(Campaigns_BroadCast_Export).click();
		
	}
	

	public void Campaigns_OnetoOneClick()
	{
		driver.findElement(Campaigns_OnetoOne).click();
	
	}
	
	public void Campaigns_OnetoOne_GatewayClick()
	{
		Select gateway = new Select(driver.findElement(Campaigns_OnetoOne_Gateway));
		gateway.selectByVisibleText("633114 -mgage(test)");
		
	}
	
	public void Campaigns_OnetoOne_ToClick(String number)
	{
		driver.findElement(Campaigns_OnetoOne_To).sendKeys(number);
		
	}
	
	public void Campaigns_OnetoOne_MessageClick(String message)
	{
		driver.findElement(Campaigns_OnetoOne_Message).sendKeys(message);
	}
	
	public void Campaigns_OnetoOne_SendClick()
	{
		driver.findElement(Campaigns_OnetoOne_Send).click();
	}
	
	public void Campaigns_RecurringClick()
	{
		driver.findElement(Campaigns_Recurring).click();
	}
	
	public void Campaigns_Recurring_CreateCampaignClick()
	{
		driver.findElement(Campaigns_Recurring_CreateCampaign).click();
	}
	
	public void Campaigns_Recurring_DeleteClick() throws Exception
	{
		driver.findElement(Campaigns_Recurring_Delete).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("(//*[@id = 'capsVal'])")).sendKeys("DELETE");
		Thread.sleep(1500);
		driver.findElement(By.xpath("(//*[text() = 'Delete'])[2]")).click();
	}
	
	
	public void Campaigns_Recurring_NamesClick()
	{
		List<org.openqa.selenium.WebElement> name = driver.findElements(Campaigns_Recurring_Names);
	    System.out.println("Total elements :"+name.size());
	              
	    String campaign_name =  ((org.openqa.selenium.WebElement) name.get(0)).getText();
		System.out.println("Name of the first campaign = "+campaign_name);
		
	}
	
	public void SuppressionList_DNDClick()
	{
		driver.findElement(SuppressionList_DND).click();
	}
	
	public void SuppressionList_DND_AddDNDNumbersClick(String number) throws Exception
	{
		driver.findElement(SuppressionList_DND_AddDNDNumbers).click();
		
		// Select values from dropdown
		Thread.sleep(2000);
		Select phonedropdown = new Select(driver.findElement(By.xpath("(//*[@name = 'field'])")));
		phonedropdown.selectByVisibleText("Phone");   // Select Phone
		
		String name = driver.findElement(By.xpath("(//*[@name = 'field'])")).getText();
		System.out.println("Name of all the options from Dropdown = "+name);
		
		driver.findElement(By.xpath("(//*[@name = 'values'])")).sendKeys(number);
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//*[@name = 'send_test_sms'])")).click(); // Add button
		Thread.sleep(2000);
	}

	public void SuppressionList_DND_ExportClick()
	{
		driver.findElement(SuppressionList_DND_Export).click();
		
		List<org.openqa.selenium.WebElement> log = driver.findElements(By.xpath("(//*[@style = 'vertical-align: middle'])"));
	    System.out.println("Total elements :"+log.size());
	              
	      String name =   ((org.openqa.selenium.WebElement) log.get(1)).getText();
	      System.out.println("Name of the exported file = "+name);
	      
	      driver.findElement(By.xpath("(//*[@style = ' vertical-align: top'])[1]")).click();    // Download 1st file
	      
	}

	public void SuppressionList_BlackListClick()
	{
		driver.findElement(SuppressionList_BlackList).click();
	
	}

	public void SuppressionList_BlackList_AddBlacklistNumbersClick(String number) throws Exception
	{
		driver.findElement(SuppressionList_BlackList_AddBlacklistNumbers).click();
		Thread.sleep(2000);

		Select phonedropdown = new Select(driver.findElement(By.xpath("(//*[@name = 'field'])")));
		phonedropdown.selectByVisibleText("Phone");
		
		String name = driver.findElement(By.xpath("(//*[@name = 'field'])")).getText();
		System.out.println("Name of all options from Selected Dropdown = "+name);
		
		driver.findElement(By.xpath("(//*[@name = 'values'])")).sendKeys(number);
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//*[@name = 'send_test_sms'])")).click(); // Add button
		Thread.sleep(2000);
	}

	public void SuppressionList_BlackList_ExportClick() throws Exception
	{
		driver.findElement(SuppressionList_BlackList_Export).click();
		
		List<org.openqa.selenium.WebElement> log = driver.findElements(By.xpath("(//*[@style = 'vertical-align: middle'])"));
	    System.out.println("Total elements :"+log.size());
	    Thread.sleep(5000);

	      String name =   ((org.openqa.selenium.WebElement) log.get(1)).getText();
	      System.out.println("Name of the exported file = "+name);
	      
	      driver.findElement(By.xpath("(//*[@style = ' vertical-align: top'])[1]")).click();    // Download 1st file
		
	}
	
	public void SuppressionList_InvalidSubscriberClick()
	{
		driver.findElement(SuppressionList_InvalidSubscriber).click();
	}

	public void SuppressionList_InvalidSubscriber_AddInvalidNumbersClick(String number) throws Exception
	{
		driver.findElement(SuppressionList_InvalidSubscriber_AddInvalidNumbers).click();
		Thread.sleep(2000);

		
		Select phonedropdown = new Select(driver.findElement(By.xpath("(//*[@name = 'field'])")));
		phonedropdown.selectByVisibleText("Phone");   // Select Phone
		
		String name = driver.findElement(By.xpath("(//*[@name = 'field'])")).getText();
		System.out.println("Name of all options from Selected Dropdown = "+name);
		
		driver.findElement(By.xpath("(//*[@name = 'values'])")).sendKeys(number);
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//*[@name = 'send_test_sms'])")).click(); // Add button
		Thread.sleep(2000);

	}

	public void SuppressionList_InvalidSubscriber_ExportClick() throws Exception
	{
		driver.findElement(SuppressionList_InvalidSubscriber_Export).click();
		List<org.openqa.selenium.WebElement> log = driver.findElements(By.xpath("(//*[@style = 'vertical-align: middle'])"));
	    System.out.println("Total elements :"+log.size());
	    Thread.sleep(5000);   
	    String name =   ((org.openqa.selenium.WebElement) log.get(1)).getText();
	    System.out.println("Name of the exported file = "+name);
	      
	    driver.findElement(By.xpath("(//*[@style = ' vertical-align: top'])[1]")).click();    // Download 1st file
	
	}

	public void PrintOverviewInfoClick()
	{
	
		List<org.openqa.selenium.WebElement> log = driver.findElements(By.xpath("(//*[@class = 'panel-title pull-left'])"));
	    System.out.println("Total elements :"+log.size());
		
	   /* Iterator<WebElement> itr = log.iterator();
	    while(itr.hasNext()) {
	    	WebElement row = itr.next();
	        System.out.println("Name of the headings are="+row.getText());
	        
	    					}*/

	    List<org.openqa.selenium.WebElement> log1 = driver.findElements(By.xpath("(//*[@class = 'pull-left font-lg'])"));
		
	   // System.out.println("Total elements :"+log1.size());
			
		  /*  Iterator<WebElement> itr1 = log1.iterator();
		    while(itr1.hasNext()) {
		    	WebElement row1 = itr1.next();
		      // System.out.println("Numeric values of the headings are="+row1.getText());
		    	System.out.println("Numeric values of the headings are="+row1.getText());*/

  		
	    for(int i=0;i<log.size();i++)
	    {
	    	System.out.println(log.get(i).getText()+" = "+log1.get(i).getText());
	    	
	    }
	    
		    						}
		    		

		    
		    
	}
	

