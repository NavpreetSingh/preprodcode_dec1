package EmailMarketingPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EM_Send_Schedule {
	public WebDriver driver;
	
		
		By Remove_Duplicate_Checkbox = By.xpath(".//*[@class='mobile_label']");
		
		By Exit_Button = By.xpath(".//*[@id='saveTemplateAndMove']");
		
		By Save_As_Draft = By.xpath("//*[text()='Save AS DRAFT']");
		
		By Save_And_Exit = By.xpath("(//*[text()='Save & Exit'])[1]");
		
		By SEND_NOW = By.xpath("//*[@id='btnSendEmail']");
		    
		By SEND_NOW_CONFIRM = By.xpath("(//*[@name = 'send'])[2]");
	
		public EM_Send_Schedule(WebDriver driver)
		{
			this.driver = driver;
		}

		
		public void Remove_Duplicate_CheckboxClick()
		{
			
			driver.findElement(Remove_Duplicate_Checkbox).click();
		}
		
		
		public void Exit_ButtonClick()
		{
			
			driver.findElement(Exit_Button).click();
		}
		
		
		public void Save_As_DraftClick()
		{
			
			driver.findElement(Save_As_Draft).click();
		}
		
		
		public void Save_And_ExitClick()
		{
			
			driver.findElement(Save_And_Exit).click();
		}
		
		
		public void SEND_NOWClick()
		{
			
			driver.findElement(SEND_NOW).click();
		}
		
		
		
		public void SEND_NOW_CONFIRMClick()
		{
			
			driver.findElement(SEND_NOW_CONFIRM).click();
		}
		
		
		
		
}
