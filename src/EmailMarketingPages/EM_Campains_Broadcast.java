package EmailMarketingPages;

import org.openqa.selenium.WebDriver;
import org.junit.validator.PublicClassValidator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EM_Campains_Broadcast {
// All codes of broadcast page
	
		WebDriver driver;
		
		public By campaign_tab=By.xpath("(//*[text()='Campaigns'])[2]"); // Click on Campaigns on Header menu
		
		public By Broadcast  = By.xpath("(//*[text()='Broadcast'])[1]"); // Click on Broadcast on Header menu
		
		public By Broadcast_name = By.xpath("//*[@title='view design']"); // Broadcast name list 
		
		public By Broadcast_Createcapmain = By.xpath("(//*[text()='CREATE CAMPAIGN'])[2]"); // Click on Create campaigns button 
				
		public By Broadcast_campaign_title = By.xpath("//*[@id = 'news_letter_name']"); // Campaign Title Box

		public By Page_name =  By.xpath("//*[@class = 'add-campaign']/h1"); // Get text of this field
	    
	   // By campaign_name = By.xpath("//*[@id = 'news_letter_name']");
	    
	    By Save_And_Next  = By.xpath("//*[text()='SAVE & NEXT STEP']");
	    
	    public By mail_chimp_server = By.xpath("//*[@class = 'zoom_img '])[1]");
	    
	    public By mail_Betaout_server = By.xpath("(//*[@class = 'zoom_img '])[2]");
	    
	    public By  Use_This_Temaplate_button = By.xpath("//*[@data-tid='389']");
	    
	    public By Preview_template = By.xpath("//*[@id='389']");
	    
	    public By EventTriggered = By.xpath("(//*[@href='/email/transactional/active'])[1]");
	    
	    public By OneToOne = By.xpath("(//*[@href='/email/one-to-one/compose'])[1]");
	    
	    public By Recurring = By.xpath("(//*[@href='/email/recurring/recurring'])[1]");
	    
	    
	    
	    
	    // Codes added for locators (Additional)
	    
	    public By broadcast_campaigns = By.xpath("//*[contains(@href,'/email-html-permalink')]");
	    public By sent_campaigns = By.xpath("//*[contains(@class,'selectable')]");
	    public By daliyCampName = By.xpath("(//*[@title = 'view design'])[1]");
	    public By dailyCampDetails = By.xpath("(//*[@class = 'hp-btn-container'])[1]");
	    public By allTemplates = By.xpath("//*[@class = 'caption']");
	    public By addEmailAddress = By.xpath("(//*[@id = 'textString'])");
	    
	    
	    public EM_Campains_Broadcast (WebDriver driver)
		{

			this.driver = driver;
		}
	    
		public void campaign_Tab (){
			
			driver.findElement(campaign_tab).click();
			
			}
		
public void Broadcast_Tab (){
			
			driver.findElement(Broadcast).click();
			
			}

public void Broadcast_Createcapmain(){
	
	driver.findElement(Broadcast_Createcapmain).click();
	
	}

public void Broadcast_campaign_title(String Broadcast_campain_title){
	
	driver.findElement(Broadcast_campaign_title).sendKeys(Broadcast_campain_title);
	
	driver.findElement(Page_name).getText();
	
	driver.findElement(Save_And_Next).click();
	
	}

public void mail_Betaout_server (){
	driver.findElement(mail_Betaout_server).click();
	
}
	
public void Template (){
	driver.findElement(Use_This_Temaplate_button).click(); // Select Template 
	
}

public void Template_preview()
{
	driver.findElement(Preview_template).click(); // Preview template button
	
}
	
public void EventTriggeredClick()
{
	driver.findElement(EventTriggered).click(); // Preview template button
	
}
	

public void OneToOneClick()
{
	driver.findElement(OneToOne).click(); // Preview template button
	
}
	

public void RecurringClick()
{
	driver.findElement(Recurring).click(); // Preview template button
	
}
	
		
	}


