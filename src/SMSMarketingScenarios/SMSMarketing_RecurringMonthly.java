package SMSMarketingScenarios;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import SMSMarketingPages.SMSMarketing_Broadcast_Active;
import SMSMarketingPages.SMSMarketing_CampaignType;
import SMSMarketingPages.SMSMarketing_Design;
import SMSMarketingPages.SMSMarketing_Homepage;
import SMSMarketingPages.SMSMarketing_Recurring_Active;
import SMSMarketingPages.SMSMarketing_Target;
import common_Classes.*;
import promocode_Pages.Onsite_CreateCampaign;
import promocode_Pages.Onsite_Select_Template;

public class SMSMarketing_RecurringMonthly {
	
	// make changes on line number - 63, 82
	
	public static WebDriver driver;

	@Test
	public void SMS_RecurringMonthly() throws Exception
	
	{
		System.out.println("Scenario : SMS Marketing > Campaigns > Recurring> Monthly > Create Campaign");
		
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
	
	//	driver.get("https://pre-prod-102.betaout.com/");
		set.getPreProdURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
	//	obj.uname("sjs22858@gmail.com");
		obj.uname(set.getShivangiUsername());
	//	obj.upswd("simranjeet");
		obj.upswd(set.getShivangiPassword());
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
		
		Homepage home_obj = new Homepage(driver);
		home_obj.sms_mktg_click();
		Thread.sleep(3000);
		
		SMSMarketing_Homepage page_obj = new SMSMarketing_Homepage(driver);
		page_obj.Campaigns_RecurringClick();
		Thread.sleep(5000);	
		page_obj.Campaigns_Recurring_CreateCampaignClick();
		
		
		SMSMarketing_CampaignType type_obj = new SMSMarketing_CampaignType(driver);
		type_obj.CampaignTypeClick();
	//	Date date1 = set.getCurrentDateTime();
		String name = "Auto_rec_monthly"+" "+System.currentTimeMillis();
		type_obj.CampaignNameClick(name);
		type_obj.PrintChooseCampaignHeading();
		type_obj.PrintCampaignNameHeading();
		type_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Design design_obj = new SMSMarketing_Design(driver);
		design_obj.ChooseGatewayClick();
		design_obj.SMSTextClick("This is test message for Recurring SMS Marketing -> Monthly");
		design_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Target target_obj = new SMSMarketing_Target(driver);
		
		target_obj.ANDConditionClick();
		target_obj.ANDDropdown1Click();
		target_obj.ANDDropdown2Click();
		target_obj.ANDDropdown3Click();
	//	target_obj.ANDDropdown4Click("987897");
		target_obj.ANDDropdown4Click("+919999315652");
		target_obj.RefreshContactsClick();
		Thread.sleep(10000);
		design_obj.SaveNextClick();
		Thread.sleep(5000);
	
		SMSMarketing_Recurring_Active rec_obj = new SMSMarketing_Recurring_Active(driver);
		rec_obj.MonthlyClick();
		
		rec_obj.Monthly_StartDateClick();
		rec_obj.Monthly_EndDateClick();
		// driver.findElement(By.xpath(("(//*[@class = 'next'])[4]"))).click();  // Next Month
		rec_obj.EndDate_NextMonthClick();
		// driver.findElement(By.xpath(("(//*[@class = 'day' and text()='10'])"))).click();  // Select 10th 
		rec_obj.EndDate_SelectDateClick();
		rec_obj.Monthly_TimeClick();
		
		set.setScheduleDateAndTime();   // To calculate -5:30hrs
		
		Thread.sleep(2000);
		rec_obj.Monthly_ExclusionDateClick();
		Thread.sleep(2000);
		rec_obj.Monthly_ExclusionDate_SelectDateClick();
		//driver.findElement(By.xpath(("(//*[@class = 'next'])[7]"))).click();  // Next Month
		rec_obj.Exclusion_NextMonthClick();
		//driver.findElement(By.xpath(("(//*[@class = 'day' and text()='10'])"))).click();  // Select 10th 
		rec_obj.Exclusion_SelectDateClick();
		rec_obj.Monthly_ExclusionDate_SaveClick();
		Thread.sleep(3000);
		
		rec_obj.Monthly_DaysToSendClick();
		Thread.sleep(3000);
		rec_obj.Monthly_DaysToSend_Weekdays_MonClick();
		rec_obj.Monthly_DaysToSend_Weekdays_TuesClick();
		rec_obj.Monthly_DaysToSend_Weekdays_WedClick();
		rec_obj.Monthly_DaysToSend_Weekdays_ThursClick();
		rec_obj.Monthly_DaysToSend_Weekdays_FriClick();
		rec_obj.Monthly_DaysToSend_Weekdays_SaveClick();
		Thread.sleep(3000);
		
		rec_obj.Monthly_PreviewClick();
		rec_obj.Monthly_ExcludingPreviewClick();
		rec_obj.Monthly_ScheduleClick();
		Thread.sleep(5000);
		

		// Assert name of campaign
		
		String MonthlyCampName = driver.findElement(By.xpath("(//*[@title = 'view design'])[1]")).getText();
		
		System.out.println("Campaign Name = " +MonthlyCampName );
		
		SoftAssert s_assert = new SoftAssert();
		s_assert.assertEquals(MonthlyCampName, name);
		s_assert.assertAll();
		System.out.println("Assertion -> Name of the campaign is matched");
		
		String campaigndetails = driver.findElement(By.xpath("(//*[@class = 'hp-btn-container'])[1]")).getText();
		System.out.println("Start Date, End date and Time of Campaign = "+campaigndetails);
		
		
	//	driver.quit();
		
		
		}

	@AfterTest
	public void kill()
	{
		driver.quit();
		
		
	}

}
	

