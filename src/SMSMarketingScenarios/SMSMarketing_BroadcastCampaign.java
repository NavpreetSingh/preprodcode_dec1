package SMSMarketingScenarios;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import SMSMarketingPages.SMSMarketing_Broadcast_Active;
import SMSMarketingPages.SMSMarketing_CampaignType;
import SMSMarketingPages.SMSMarketing_Design;
import SMSMarketingPages.SMSMarketing_Homepage;
import SMSMarketingPages.SMSMarketing_Target;
import common_Classes.*;
import common_Classes.Account_Settings;
import common_Classes.Loginpage;
import promocode_Pages.Onsite_CreateCampaign;
import common_Classes.Onsite_Design;
import common_Classes.Homepage;
import promocode_Pages.Onsite_Select_Template;
import common_Classes.Onsite_Target_Page;
	
public class SMSMarketing_BroadcastCampaign{
	
	// make changes on line number - 64, 83

	public static WebDriver driver;

	@Test
	public void SMS_BroadcastCampaign() throws Exception
	
	{
		System.out.println("Scenario : SMS Marketing > Campaigns > Broadcast > Create Campaign");
		
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
	
	//	driver.get("https://pre-prod-102.betaout.com/");
		set.getPreProdURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
	//	obj.uname("sjs22858@gmail.com");
		obj.uname(set.getShivangiUsername());
	//	obj.upswd("simranjeet");
		obj.upswd(set.getShivangiPassword());
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
		
		Homepage home_obj = new Homepage(driver);
		home_obj.sms_mktg_click();
		Thread.sleep(3000);
		
		SMSMarketing_Homepage page_obj = new SMSMarketing_Homepage(driver);
		page_obj.Campaigns_BroadCast_CreateCampaignClick();
		
		SMSMarketing_CampaignType type_obj = new SMSMarketing_CampaignType(driver);
		type_obj.CampaignTypeClick();
	//	Date date1 = set.getCurrentDateTime();
		String name = "Auto_broadcast"+" "+System.currentTimeMillis();
		type_obj.CampaignNameClick(name);
		type_obj.PrintChooseCampaignHeading();
		type_obj.PrintCampaignNameHeading();
		type_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Design design_obj = new SMSMarketing_Design(driver);
		design_obj.ChooseGatewayClick();
		design_obj.SMSTextClick("This is test message for sms marketing");
		design_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Target target_obj = new SMSMarketing_Target(driver);
		
		target_obj.ANDConditionClick();
		target_obj.ANDDropdown1Click();
		target_obj.ANDDropdown2Click();
		target_obj.ANDDropdown3Click();
	//	target_obj.ANDDropdown4Click("987897");
		target_obj.ANDDropdown4Click("+919999315652");
		target_obj.RefreshContactsClick();
		Thread.sleep(8000);
		design_obj.SaveNextClick();
		Thread.sleep(5000);
		
		SMSMarketing_Broadcast_Active active_obj = new SMSMarketing_Broadcast_Active(driver);
		active_obj.SendNowClick();
		Thread.sleep(9000);
		

		// Assert name of campaign
		
		String BroadcastCampName = driver.findElement(By.xpath("(//*[contains(@href,'/sms/permalink/nv/smsTextId')])[1]")).getText();
		
		System.out.println("Campaign Name = " +BroadcastCampName );
		
		SoftAssert s_assert = new SoftAssert();
		s_assert.assertEquals(BroadcastCampName, name);
		s_assert.assertAll();
		System.out.println("Assertion -> Name of the campaign is matched");
		
	//	driver.quit();
		
	}
	
	@AfterTest
	public void kill()
	{
		driver.quit();
		
		
	}
	
}