package OnsiteMarketingScenarios_CTA;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import EmailMarketingPages.EM_Broadcast_Template;
import common_Classes.*;
import common_Classes.Account_Settings;
import common_Classes.Loginpage;
import OnsiteMarketingPages.Onsite_CreateCampaign;
import OnsiteMarketingPages.Onsite_Design;
import common_Classes.Homepage;
import OnsiteMarketingPages.Onsite_Select_Template;
import OnsiteMarketingPages.Onsite_Target_Page;

public class Test1 {

	public static WebDriver driver;
	
	// Before running scenario, make changes on line number - 61
	
	
	@Test
	public void CallToActionBanner() throws Exception
	
	{
		System.out.println("Scenario : Onsite Marketing > Select CTABannerTemplate > Design > Target > Active");
		
	
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
	
		
		Date d1 = set.getCurrentDateTime();
		System.out.println(d1);
		
		
	//	set.getPreProdURL();
		
	//	driver.get("https://pre-prod-102.betaout.com/");
		
		set.getPreProdURL();
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
	//	obj.uname(set.getWoocommercetestUsername());
	//	obj.uname("Woocommercetest@y0v.in");
		obj.uname(set.getShivangiUsername());
	//	obj.upswd("Woocommercetest");
		obj.upswd(set.getShivangiPassword());
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
		
		Homepage onsite_obj = new Homepage(driver);
		onsite_obj.onsite_click();
		
		Onsite_CreateCampaign calltoaction_obj = new Onsite_CreateCampaign(driver);
		
		calltoaction_obj.PrintNameOfFields();
	//	calltoaction_obj.PrintCampaignPopupDetails();
		
		calltoaction_obj.createcampaign_click();
	//	Date date1 = set.getCurrentDateTime();
		String name = "Auto_ctabanner"+" "+System.currentTimeMillis();
		calltoaction_obj.name(name);
		Thread.sleep(1500);
		calltoaction_obj.save_click();
		Thread.sleep(3000);
		
		Onsite_Select_Template template_obj = new Onsite_Select_Template(driver);
		template_obj.BannerClick();            // Select banner
		Thread.sleep(3000);
		template_obj.CTABannertemplateClick();
		
		
		// Explicit wait for Save and Next after selecting template
		
		EM_Broadcast_Template timeobj = new EM_Broadcast_Template(driver);
		
		
		
		 WebDriverWait wait = new WebDriverWait(driver,15);
 	//	 wait.until(ExpectedConditions.visibilityOfElementLocated(timeobj.getCampaign_name()));
					 
	//	Thread.sleep(18000);
		 Thread.sleep(8000);
		 
	}
	
}