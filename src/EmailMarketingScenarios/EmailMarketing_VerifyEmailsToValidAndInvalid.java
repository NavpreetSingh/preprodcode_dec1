package EmailMarketingScenarios;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import CartRecoveryPages.CartRecovery_Homepage;
import EmailMarketingPages.EM_Broadcast_Template;
import EmailMarketingPages.EM_Campains_Broadcast;
import EmailMarketingPages.EM_Send_Schedule;
import EmailMarketingPages.EM_Tageting_Page;
import EmailMarketingPages.EmailMarketing_Supression;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;

public class EmailMarketing_VerifyEmailsToValidAndInvalid {


	public static WebDriver driver;
	
	
	
	@Test
	public void Email_SupressionAndBroadcast() throws Exception{
		
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
				
		// Code to load the Property file
		
		Properties prop = set.loadPropertyFile();
	
		set.getPreProdURL(); 
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getShivangiUsername());
		obj.upswd(set.getShivangiPassword());
		
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
				
		Homepage homeobj = new Homepage(driver);
		homeobj.email_mktg_click();
		
		// Add Unsubscribed Contact
		
		EM_Broadcast_Template obj1 = new EM_Broadcast_Template(driver);
		obj1.SupressionListClick();
		
		EmailMarketing_Supression obj2 = new EmailMarketing_Supression(driver);
		obj2.Unsubscribers_AddNewClick();
		Thread.sleep(2000);
		
	//	String actualUnsubscribedEmail = System.currentTimeMillis()+""+"U@yopmail.com";
		String actualUnsubscribedEmail = System.currentTimeMillis()+ prop.getProperty("Email_Unsubscribed_EmailID");
		
		obj2.Unsubscribe_EnterEmailsClick(actualUnsubscribedEmail);
		
		obj2.Unsubscribe_AddToUnsubscribeListClick();
		Thread.sleep(2000);
		
		String expectedUnsubscribedEmail = 	obj2.PrintUnsubscribedEmailAddress();
		
		obj2.PrintUnsubscribedEmailTime();
		
		SoftAssert sa_unsubscribe = new SoftAssert();
		sa_unsubscribe.assertEquals(actualUnsubscribedEmail, expectedUnsubscribedEmail);
		sa_unsubscribe.assertAll();
	
		System.out.println("Assertion of Unsubscribed email address is done successfully");
	
		// Add Spam Contact
	
		obj2.Spam_ComplaintsClick();
		Thread.sleep(2000);
		
		obj2.Spam_AddNewClick();
		Thread.sleep(2000);
		
	//	String actualSpamEmail = System.currentTimeMillis()+""+"S@yopmail.com";
		String actualSpamEmail = System.currentTimeMillis()+ prop.getProperty("Email_Spam_EmailID");
		
		obj2.Spam_EnterEmailsClick(actualSpamEmail);
		
		obj2.Spam_AddToUnsubscribeListClick();
		Thread.sleep(2000);
	
		obj2.UnsubscribersClick();
		Thread.sleep(2000);
			
		String expectedSpamEmail = 	obj2.PrintBouncedEmailAddress();
		
		obj2.PrintBouncedEmailTime();
		
		SoftAssert sa_spam = new SoftAssert();
		sa_spam.assertEquals(actualSpamEmail, expectedSpamEmail);
		sa_spam.assertAll();
	
		System.out.println("Assertion of Spam email address is done successfully");
		
		// Add Bounced Contact
	
		obj2.BouncedClick();
		Thread.sleep(2000);
		
		obj2.Bounced_AddNewClick();
		Thread.sleep(2000);
		
	//	String actualBouncedEmail = System.currentTimeMillis()+""+"B@yopmail.com";
		String actualBouncedEmail = System.currentTimeMillis()+ prop.getProperty("Email_Bounced");
		
		obj2.Bounced_EnterEmailsClick(actualBouncedEmail);
		
		obj2.Bounced_AddToUnsubscribeListClick();
		Thread.sleep(2000);
		
		obj2.UnsubscribersClick();
		Thread.sleep(2000);
		
		String expectedBouncedEmail = 	obj2.PrintBouncedEmailAddress();
		
		obj2.PrintBouncedEmailTime();
		
		SoftAssert sa_bounced = new SoftAssert();
		sa_bounced.assertEquals(actualBouncedEmail, expectedBouncedEmail);
		sa_bounced.assertAll();
	
		System.out.println("Assertion of Bounced email address is done successfully");
	
		// Send Broadcast
		
		EM_Campains_Broadcast obj10 = new EM_Campains_Broadcast(driver);
		obj10.campaign_Tab();
		Thread.sleep(2000);
		obj10.Broadcast_Createcapmain();
		Thread.sleep(2000);
	//	String name = "Email_broadcast"+" "+System.currentTimeMillis();
		String name = prop.getProperty("Email_Broadcast_Name") +System.currentTimeMillis();
				
		obj10.Broadcast_campaign_title(name);
		Thread.sleep(3000);
	
	//	obj10.mail_Betaout_server();
		Thread.sleep(2000);
		obj10.Template();
		Thread.sleep(15000);
	
		EM_Broadcast_Template obj20 = new EM_Broadcast_Template(driver);
	//	String subject_data = "Test Subject"+""+System.currentTimeMillis();
		String subject_data =  prop.getProperty("Email_Broadcast_Subject") + System.currentTimeMillis();
		obj20.Campaign_Subject(subject_data);
		
		obj20.Save_and_NextStep();
		Thread.sleep(2000);
		obj20.Linkurl_PopUp_OK();
		Thread.sleep(10000);
	
		EM_Tageting_Page obj30 = new EM_Tageting_Page(driver);
	//	String email1 = System.currentTimeMillis()+""+"Z@yopmail.com";
		String email1 = System.currentTimeMillis()+ prop.getProperty("Email_VerifyValidInvalidEmailID1");
	//	String email2 = System.currentTimeMillis()+""+"Y@yopmail.com";
		String email2 = System.currentTimeMillis()+ prop.getProperty("Email_VerifyValidInvalidEmailID2");
		
		
		obj30.AddMoreEmailAddressClick(email1);

		driver.findElement(obj10.addEmailAddress).sendKeys(Keys.ENTER);		
		obj30.AddMoreEmailAddressClick(email2);
		driver.findElement(obj10.addEmailAddress).sendKeys(Keys.ENTER);	
		obj30.AddMoreEmailAddressClick(actualUnsubscribedEmail);
		driver.findElement(obj10.addEmailAddress).sendKeys(Keys.ENTER);	
		obj30.AddMoreEmailAddressClick(actualSpamEmail);
		driver.findElement(obj10.addEmailAddress).sendKeys(Keys.ENTER);	
		obj30.AddMoreEmailAddressClick(actualBouncedEmail);
		Thread.sleep(2000);
		
		// Add Supression List Emails
		
		obj30.SaveandNextClick();
		Thread.sleep(10000);
		
		EM_Send_Schedule obj40 = new EM_Send_Schedule(driver);
		obj40.SEND_NOWClick();
		Thread.sleep(2000);
		
		obj40.SEND_NOW_CONFIRMClick();
		Thread.sleep(10000);
		
		List<org.openqa.selenium.WebElement> broadcast_campaigns = driver.findElements(obj10.broadcast_campaigns);
		System.out.println("Total number of campaigns are = "+broadcast_campaigns.size());
		
		String campaignname = ((org.openqa.selenium.WebElement) broadcast_campaigns.get(0)).getText();
		System.out.println("Name of the created campaign = "+campaignname);
		
		
		SoftAssert sa_Broadcast = new SoftAssert();
		sa_Broadcast.assertEquals(campaignname,name);
		sa_Broadcast.assertAll();
	
		
		Thread.sleep(8000);
		
		// Open new tab and verify subject of the 1st email
		
		
	//	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");   	// Open new tab
		
		((JavascriptExecutor) driver).executeScript("window.open('','_blank');");
		Thread.sleep(2000);
		
		ArrayList<String> tabs1 = new ArrayList<String> (driver.getWindowHandles());
		 
		driver.switchTo().window(tabs1.get(1)); //switches to first tab	
		set.getYopmailURL();
		
		Thread.sleep(5000);
		
		CartRecovery_Homepage carthome_obj = new CartRecovery_Homepage(driver);
		
		driver.findElement(carthome_obj.YopmailAccount_Username).sendKeys(email1);     // Enter username
		driver.findElement(carthome_obj.YopmailAccount_CheckInbox).click();   // Click on Check Inbox
		Thread.sleep(5000);
		
		driver.switchTo().frame(driver.findElement(carthome_obj.YopmailAccount_Iframe));
		
		List<org.openqa.selenium.WebElement> totalemails1 = driver.findElements(carthome_obj.YopmailAccount_TotalEmails);
		System.out.println("Total number of emails = "+totalemails1.size());
			
		String firstemailsubject = ((org.openqa.selenium.WebElement) totalemails1.get(0)).getText();
		System.out.println("Name of the created campaign = "+firstemailsubject);
		
		sa_Broadcast.assertEquals(firstemailsubject,subject_data);
		sa_Broadcast.assertAll();
		
		System.out.println("Name of the subject is verified ");
		Thread.sleep(8000);

		
		// Open new tab and verify subject of the 2nd email
		
		
		//		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");   	// Open new tab
			
		((JavascriptExecutor) driver).executeScript("window.open('','_blank');");
		Thread.sleep(2000);
				
				ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
				 
				driver.switchTo().window(tabs2.get(2)); //switches to second tab

				set.getYopmailURL();
				Thread.sleep(5000);
				
	
				driver.findElement(carthome_obj.YopmailAccount_Username).sendKeys(email2);     // Enter username
				driver.findElement(carthome_obj.YopmailAccount_CheckInbox).click();   // Click on Check Inbox
				Thread.sleep(5000);
				
				driver.switchTo().frame(driver.findElement(carthome_obj.YopmailAccount_Iframe));
				
				List<org.openqa.selenium.WebElement> totalemails2 = driver.findElements(carthome_obj.YopmailAccount_TotalEmails);
				System.out.println("Total number of emails = "+totalemails2.size());
								
				String firstemailsubject1 = ((org.openqa.selenium.WebElement) totalemails2.get(0)).getText();
				System.out.println("Name of the created campaign = "+firstemailsubject1);
				
				sa_Broadcast.assertEquals(firstemailsubject1,subject_data);
				sa_Broadcast.assertAll();
				
				System.out.println("Name of the subject is verified ");
				Thread.sleep(8000);
		
		
				// Open new tab and verify subject of the Unsubscribed email
				
				
			//	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");   	// Open new tab
				
				((JavascriptExecutor) driver).executeScript("window.open('','_blank');");	
				Thread.sleep(2000);
				
				ArrayList<String> tabs3 = new ArrayList<String> (driver.getWindowHandles());
				 
				driver.switchTo().window(tabs3.get(3)); //switches to second tab
				
				set.getYopmailURL();
				
				Thread.sleep(5000);
				
				driver.findElement(carthome_obj.YopmailAccount_Username).sendKeys(actualUnsubscribedEmail);     // Enter username
				driver.findElement(carthome_obj.YopmailAccount_CheckInbox).click();   // Click on Check Inbox
				Thread.sleep(5000);
				
				driver.switchTo().frame(driver.findElement(carthome_obj.YopmailAccount_Iframe));
				
				List<org.openqa.selenium.WebElement> totalemails3 = driver.findElements(carthome_obj.YopmailAccount_TotalEmails);
				System.out.println("Total number of emails = "+totalemails3.size());
				
				/*String firstemailsubject1 = ((org.openqa.selenium.WebElement) totalemails1.get(0)).getText();
				System.out.println("Name of the created campaign = "+firstemailsubject1);
				*/
				
				if(totalemails3.size() == 0)
					
				{
					System.out.println("No email is present as user is Unsubscribed");
					
				}
				
				else
				{

					System.out.println("Emails are present for Unsubscribed user");
					
				}
	
				Thread.sleep(7000);
				
				// Open new tab and verify subject of the Spam email
				
				
			//	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");   	// Open new tab
				
				((JavascriptExecutor) driver).executeScript("window.open('','_blank');");
				Thread.sleep(2000);
				
				ArrayList<String> tabs4 = new ArrayList<String> (driver.getWindowHandles());
				 
				driver.switchTo().window(tabs4.get(4)); //switches to second tab
		
				set.getYopmailURL();
				
				Thread.sleep(5000);
				
				driver.findElement(carthome_obj.YopmailAccount_Username).sendKeys(actualSpamEmail);     // Enter username
				driver.findElement(carthome_obj.YopmailAccount_CheckInbox).click();   // Click on Check Inbox
				Thread.sleep(5000);
				
				driver.switchTo().frame(driver.findElement(carthome_obj.YopmailAccount_Iframe));
				
				List<org.openqa.selenium.WebElement> totalemails4 = driver.findElements(carthome_obj.YopmailAccount_TotalEmails);
				System.out.println("Total number of emails = "+totalemails4.size());
				
				/*String firstemailsubject1 = ((org.openqa.selenium.WebElement) totalemails1.get(0)).getText();
				System.out.println("Name of the created campaign = "+firstemailsubject1);
				*/
				
				if(totalemails4.size() == 0)
					
				{
					System.out.println("No email is present as user is Spam user");
					
				}
				
				else
				{

					System.out.println("Emails are present for Spam user");
					
				}
			
				
				Thread.sleep(7000);
				
				// Open new tab and verify subject of the Bounced email
				
				
			//	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");   	// Open new tab
				
				((JavascriptExecutor) driver).executeScript("window.open('','_blank');");
				Thread.sleep(2000);
				
				ArrayList<String> tabs5 = new ArrayList<String> (driver.getWindowHandles());
				 
				driver.switchTo().window(tabs5.get(5)); //switches to second tab
					
			//	driver.get("http://www.yopmail.com/en/");
			//	driver.get(prop.getProperty("Yopmail_URL"));
				
				set.getYopmailURL();
				Thread.sleep(5000);
				
				driver.findElement(carthome_obj.YopmailAccount_Username).sendKeys(actualBouncedEmail);     // Enter username
				driver.findElement(carthome_obj.YopmailAccount_CheckInbox).click();   // Click on Check Inbox
				Thread.sleep(5000);
				
				driver.switchTo().frame(driver.findElement(carthome_obj.YopmailAccount_Iframe));
				
				List<org.openqa.selenium.WebElement> totalemails5 = driver.findElements(carthome_obj.YopmailAccount_TotalEmails);
				System.out.println("Total number of emails = "+totalemails5.size());
					
				/*String firstemailsubject1 = ((org.openqa.selenium.WebElement) totalemails1.get(0)).getText();
				System.out.println("Name of the created campaign = "+firstemailsubject1);
				*/
				
				if(totalemails5.size() == 0)
					
				{
					System.out.println("No email is present as user is Bounced User");
					
				}
				
				else
				{

					System.out.println("Emails are present for Bounced User");
					
				}
			
				
				
	//			driver.quit();
				
	
		}

	@AfterTest
	public void kill()
	{
		driver.quit();
		
		
	}
	
}