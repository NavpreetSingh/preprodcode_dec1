package EmailMarketingScenarios;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import CartRecoveryPages.CartRecovery_Homepage;
import EmailMarketingPages.EM_Broadcast_Template;
import EmailMarketingPages.EM_Campains_Broadcast;
import EmailMarketingPages.EM_Tageting_Page;
import EmailMarketingPages.EmailMarketing_OnetoOne;
import EmailMarketingPages.EmailMarketing_RecurringActive;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;

public class EmailMarketing_Recurring_Monthly {

	
	public static WebDriver driver;
	
	
	
	@Test
	public void RecurringCampaign_Monthy() throws Exception{
		
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
				
		// Code to load the Property file
		
		Properties prop = set.loadPropertyFile();
		
		set.getPreProdURL(); 
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
	
		obj.uname(set.getShivangiUsername());
	
		obj.upswd(set.getShivangiPassword());
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
				
		Homepage homeobj = new Homepage(driver);
		homeobj.email_mktg_click();
		
		EM_Campains_Broadcast obj1 = new EM_Campains_Broadcast(driver);
		obj1.campaign_Tab();
		Thread.sleep(2000);
		obj1.RecurringClick();
		Thread.sleep(6000);
		
		obj1.Broadcast_Createcapmain();
		Thread.sleep(2000);
		String name = prop.getProperty("Email_RecurringMonthly_Name") +System.currentTimeMillis();
		
		obj1.Broadcast_campaign_title(name);
		Thread.sleep(5000);

		obj1.Template();
		Thread.sleep(15000);
	
		EM_Broadcast_Template obj2 = new EM_Broadcast_Template(driver);
		String subject_data = prop.getProperty("Email_RecurringMonthly_Subject") +System.currentTimeMillis();
		
		obj2.Campaign_Subject(subject_data);
		
		obj2.Save_and_NextStep();
		Thread.sleep(2000);
		obj2.Linkurl_PopUp_OK();
		Thread.sleep(10000);
		
		
		EM_Tageting_Page obj3 = new EM_Tageting_Page(driver);
		String email = System.currentTimeMillis() + prop.getProperty("Email_RecurringMonthly_EmailID");
		
		obj3.AddMoreEmailAddressClick(email);
		obj3.SaveandNextClick();
		Thread.sleep(10000);

		EmailMarketing_RecurringActive rec_obj = new EmailMarketing_RecurringActive(driver);
		
		rec_obj.MonthlyClick();
		
		rec_obj.Monthly_StartDateClick();
		rec_obj.Monthly_EndDateClick();
		rec_obj.EndDate_NextMonthClick();
		rec_obj.EndDate_SelectDateClick();
		rec_obj.Monthly_TimeClick();
		
		set.setScheduleDateAndTime();   // To calculate -5:30hrs
		
		Thread.sleep(2000);
		rec_obj.Monthly_ExclusionDateClick();
		Thread.sleep(2000);
		rec_obj.Monthly_ExclusionDate_SelectDateClick();
		rec_obj.Exclusion_NextMonthClick();
		rec_obj.Exclusion_SelectDateClick();
		rec_obj.Monthly_ExclusionDate_SaveClick();
		Thread.sleep(3000);
		
		rec_obj.Monthly_DaysToSendClick();
		Thread.sleep(3000);
		rec_obj.Monthly_DaysToSend_Weekdays_MonClick();
		rec_obj.Monthly_DaysToSend_Weekdays_TuesClick();
		rec_obj.Monthly_DaysToSend_Weekdays_WedClick();
		rec_obj.Monthly_DaysToSend_Weekdays_ThursClick();
		rec_obj.Monthly_DaysToSend_Weekdays_FriClick();
		rec_obj.Monthly_DaysToSend_Weekdays_SaveClick();
		Thread.sleep(3000);
		
		rec_obj.Monthly_PreviewClick();
		rec_obj.Monthly_ExcludingPreviewClick();
		rec_obj.Monthly_ScheduleClick();
		Thread.sleep(5000);


		// Assert name of campaign
		
		String MonthlyCampName = driver.findElement(obj1.daliyCampName).getText();		
		System.out.println("Campaign Name = " +MonthlyCampName);
		
		SoftAssert s_assert = new SoftAssert();
		s_assert.assertEquals(MonthlyCampName, name);
		s_assert.assertAll();
		System.out.println("Assertion -> Name of the campaign is matched");
		
		String campaigndetails = driver.findElement(obj1.dailyCampDetails).getText();
		System.out.println("Start Date, End date and Time of Campaign = "+campaigndetails);
		Thread.sleep(8000);
		
		// Open new tab and verify subject of the email
		
		
	//	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");   	// Open new tab
		
		((JavascriptExecutor) driver).executeScript("window.open('','_blank');");
		Thread.sleep(2000);
		
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		 
		driver.switchTo().window(tabs.get(1)); //switches to first tab

		set.getYopmailURL();
		
		Thread.sleep(180000);           // Wait for 3 min 
		
		CartRecovery_Homepage carthome_obj = new CartRecovery_Homepage(driver);
		
		driver.findElement(carthome_obj.YopmailAccount_Username).sendKeys(email);     // Enter username
		driver.findElement(carthome_obj.YopmailAccount_CheckInbox).click();   // Click on Check Inbox
		Thread.sleep(5000);
		
		driver.switchTo().frame(driver.findElement(carthome_obj.YopmailAccount_Iframe));
		
		List<org.openqa.selenium.WebElement> totalemails = driver.findElements(carthome_obj.YopmailAccount_TotalEmails);
		System.out.println("Total number of emails = "+totalemails.size());
		
		String firstemailsubject = ((org.openqa.selenium.WebElement) totalemails.get(0)).getText();
		System.out.println("Name of the created campaign = "+firstemailsubject);
		
		s_assert.assertEquals(firstemailsubject,subject_data);
		s_assert.assertAll();
		
		System.out.println("Name of the subject is verified ");
		
//		driver.quit();
		

	
}
	
	@AfterTest
	public void kill()
	{
		driver.quit();
		
		
	}
	
}