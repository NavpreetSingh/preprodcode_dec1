package EmailMarketingScenarios;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import CartRecoveryPages.CartRecovery_Homepage;
import EmailMarketingPages.EM_Campains_Broadcast;
import EmailMarketingPages.EmailMarketing_OnetoOne;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;

public class EmailMarketing_OneToOneCampaign {

public static WebDriver driver;
	
	
	@Test
	public void OneToOneCampaign() throws Exception{
		
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
		
		// Code to load the Property file
	
		Properties prop = set.loadPropertyFile();
		
		set.getPreProdURL(); 
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getShivangiUsername());
		obj.upswd(set.getShivangiPassword());
		
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
				
		Homepage homeobj = new Homepage(driver);
		homeobj.email_mktg_click();
		
		EM_Campains_Broadcast obj1 = new EM_Campains_Broadcast(driver);
		obj1.campaign_Tab();
		Thread.sleep(2000);
		obj1.OneToOneClick();
		Thread.sleep(6000);
		
		EmailMarketing_OnetoOne obj11 = new EmailMarketing_OnetoOne(driver);
		obj11.FromClick();
		obj11.TemplateClick();

		String Subject = prop.getProperty("Email_OneToOne_Subject") +System.currentTimeMillis();
		obj11.SubjectClick(Subject);
		
		String ToEmail = System.currentTimeMillis() + prop.getProperty("Email_OneToOne_EmailID");
		obj11.ToClick(ToEmail);
		
		String message = prop.getProperty("Email_OneToOne_Message") + System.currentTimeMillis();
		obj11.MessageClick(message);
	
		obj11.SendClick();
		Thread.sleep(10000);
		
		List<org.openqa.selenium.WebElement> sent_campaigns = driver.findElements(obj1.sent_campaigns);
		System.out.println("Total number of sent campaigns are = "+sent_campaigns.size());
				
		
	//	List<org.openqa.selenium.WebElement> names = driver.findElements(By.xpath("//*[contains(@class,'selectable')]/td[1]"));
		
	//	String emailid = ((org.openqa.selenium.WebElement) names.get(0)).getText();
		
/*		List<org.openqa.selenium.WebElement> subjects = driver.findElements(By.xpath("//*[contains(@class,'selectable')]/td[2]"));
		String subject1 = ((org.openqa.selenium.WebElement) subjects.get(0)).getText();
		System.out.println("Subject of the sent campaign = "+subject1);
		*/
		
		SoftAssert s_assert = new SoftAssert();
		/*s_assert.assertEquals(emailid,ToEmail);
		s_assert.assertAll();

		System.out.println("Assertion of Email Receiver is done");
	
		s_assert.assertEquals(subject1,Subject);
		s_assert.assertAll();

		System.out.println("Assertion of Subject of email is done");*/
		

		// Open new tab and verify subject of the email
		
		Thread.sleep(5000);
		
	//	driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");   	// Open new tab
		
		((JavascriptExecutor) driver).executeScript("window.open('','_blank');");
		Thread.sleep(2000);
		
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		 
		driver.switchTo().window(tabs.get(1)); //switches to first tab

		set.getYopmailURL();
		
		Thread.sleep(5000);
		
		CartRecovery_Homepage carthome_obj = new CartRecovery_Homepage(driver);
		
		driver.findElement(carthome_obj.YopmailAccount_Username).sendKeys(ToEmail);     // Enter username
		driver.findElement(carthome_obj.YopmailAccount_CheckInbox).click();   // Click on Check Inbox
		Thread.sleep(5000);
		
		driver.switchTo().frame(driver.findElement(carthome_obj.YopmailAccount_Iframe));
		
		
		List<org.openqa.selenium.WebElement> totalemails = driver.findElements(carthome_obj.YopmailAccount_TotalEmails);
		System.out.println("Total number of emails = "+totalemails.size());
		
		String firstemailsubject = ((org.openqa.selenium.WebElement) totalemails.get(0)).getText();
		System.out.println("Subject of the sent campaign = "+firstemailsubject);
		
		s_assert.assertEquals(firstemailsubject,Subject);
		s_assert.assertAll();
		
		System.out.println("Subject of the email is verified ");
		
		driver.switchTo().parentFrame();
		
		driver.switchTo().frame(driver.findElement(carthome_obj.YopmailAccount_IframeText));
		
		String emailmessage = 	driver.findElement(carthome_obj.YopmailAccount_Text).getText();
		System.out.println("Message written in the email = "+emailmessage);
		
		s_assert.assertEquals(message,emailmessage);
		s_assert.assertAll();
		
		System.out.println("Text of the email is verified ");
		
	//	driver.quit();
			

	
												}
	
	@AfterTest
	public void kill()
	{
		driver.quit();
		
		
	}
}
