package EmailMarketingScenarios;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import CartRecoveryPages.CartRecovery_Homepage;
import EmailMarketingPages.EM_Broadcast_Template;
import EmailMarketingPages.EM_Campains_Broadcast;
import EmailMarketingPages.EM_Send_Schedule;
import EmailMarketingPages.EM_Tageting_Page;
import common_Classes.Homepage;
import common_Classes.Loginpage;
import common_Classes.Setup_class;

public class EmailMarketing_Broadcast {

public static WebDriver driver;
	
	
	@Test
	public void EmailMarketing_BroadcastCampaign() throws Exception{
		
		Setup_class set = new Setup_class(driver);
		
		driver = set.setup();
				
		// Code to load the Property file
		
		Properties prop = set.loadPropertyFile();
		set.getPreProdURL(); 
		
		Loginpage obj = new Loginpage(driver);
		
		obj.loginbutton();
		obj.uname(set.getShivangiUsername());
		obj.upswd(set.getShivangiPassword());
		
		obj.ulogin();
		
		System.out.println("User has logged in successfully");
				
		Homepage homeobj = new Homepage(driver);
		homeobj.email_mktg_click();
		
		EM_Campains_Broadcast obj1 = new EM_Campains_Broadcast(driver);
		obj1.campaign_Tab();
		Thread.sleep(2000);
		obj1.Broadcast_Createcapmain();
		Thread.sleep(2000);
		
		String name = prop.getProperty("Email_Broadcast_Name") + System.currentTimeMillis();
		
		obj1.Broadcast_campaign_title(name);
		Thread.sleep(3000);
	
//		obj1.mail_Betaout_server();       // needed when Mailchimp is Enabled from Settings
		
		Thread.sleep(2000);
		obj1.Template();
		Thread.sleep(15000);
	
		EM_Broadcast_Template obj2 = new EM_Broadcast_Template(driver);
		
		String subject_data = prop.getProperty("Email_Broadcast_Subject") + System.currentTimeMillis();
		
		obj2.Campaign_Subject(subject_data);
		
		obj2.Save_and_NextStep();
		Thread.sleep(2000);
		obj2.Linkurl_PopUp_OK();
		Thread.sleep(10000);
	
		EM_Tageting_Page obj3 = new EM_Tageting_Page(driver);
		String email = System.currentTimeMillis() + prop.getProperty("Email_Broadcast_EmailID");
		
		obj3.AddMoreEmailAddressClick(email);
		obj3.SaveandNextClick();
		Thread.sleep(10000);
		
		EM_Send_Schedule obj4 = new EM_Send_Schedule(driver);
		obj4.SEND_NOWClick();
		Thread.sleep(2000);
		
		obj4.SEND_NOW_CONFIRMClick();
		Thread.sleep(10000);
		
		// Start making changes from here
		
	//	List<org.openqa.selenium.WebElement> broadcast_campaigns = driver.findElements(By.xpath("//*[contains(@href,'/email-html-permalink')]"));
		List<org.openqa.selenium.WebElement> broadcast_campaigns = driver.findElements(obj1.broadcast_campaigns);
		
		
		System.out.println("Total number of campaigns are = "+broadcast_campaigns.size());
		
		String campaignname = ((org.openqa.selenium.WebElement) broadcast_campaigns.get(0)).getText();
		System.out.println("Name of the created campaign = "+campaignname);
		
		
		SoftAssert s_assert = new SoftAssert();
		s_assert.assertEquals(campaignname,name);
		s_assert.assertAll();
		Thread.sleep(6000);
		
		// Open new tab and verify subject of the email

//		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");   	// Opens new tab
	
		((JavascriptExecutor) driver).executeScript("window.open('','_blank');");
		
		Thread.sleep(2000);
		
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		 
		driver.switchTo().window(tabs.get(1)); //switches to first tab
			
	//	driver.get("http://www.yopmail.com/en/");
		
		set.getYopmailURL();
		
		Thread.sleep(5000);
		
		CartRecovery_Homepage carthome_obj = new CartRecovery_Homepage(driver);
		
		driver.findElement(carthome_obj.YopmailAccount_Username).sendKeys(email);     // Enter username
		driver.findElement(carthome_obj.YopmailAccount_CheckInbox).click();   // Click on Check Inbox
		Thread.sleep(5000);
		
		driver.switchTo().frame(driver.findElement(carthome_obj.YopmailAccount_Iframe));
		
		List<org.openqa.selenium.WebElement> totalemails = driver.findElements(carthome_obj.YopmailAccount_TotalEmails);
		System.out.println("Total number of emails = "+totalemails.size());
		
		String firstemailsubject = ((org.openqa.selenium.WebElement) totalemails.get(0)).getText();
		System.out.println("Name of the created campaign = "+firstemailsubject);
		
		s_assert.assertEquals(firstemailsubject,subject_data);
		s_assert.assertAll();
		
		System.out.println("Name of the subject is verified ");
		
	//	driver.quit();
		
		
	
												}
	
	@AfterTest
	public void kill()
	{
		driver.quit();
		
		
	}
}
