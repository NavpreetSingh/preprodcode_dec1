package common_Classes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class Setup_class	{

	
	public WebDriver driver;
	
 public WebDriver setup() throws Exception {

		System.setProperty("webdriver.chrome.driver", "Resources\\chromedriver.exe");
		
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-extensions");
			driver = new ChromeDriver(options);
		
	//	driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				
		/* FileInputStream fileInput = new FileInputStream(new File("src\\data.properties"));	 		 
		 Properties prop = new Properties();
		 prop.load(fileInput);
*/
		
		return driver;					
}

 
 public Properties loadPropertyFile() throws Exception
 {
	 FileInputStream fileInput = new FileInputStream(new File("Resources\\data.properties"));	 		 
	 Properties prop = new Properties();
	 prop.load(fileInput);
	 
	 return prop;
 }
 
	public Date getCurrentDateTime()
	
	{
	
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date1 = new Date();
		dateFormat.format(date1);
		return date1;
	}
	

	/*
	public void setScheduleDateAndTime() throws Exception
	{
		 		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
		Date scheduleDate = new Date((System.currentTimeMillis() - TimeUnit.HOURS.toMillis(5)) - TimeUnit.MINUTES.toMillis(27));

		System.out.println(dateFormat.format(scheduleDate));
		
		String date1 = dateFormat.format(scheduleDate);
		System.out.println(date1);
		Date currentDate = new Date(System.currentTimeMillis());
		String todayDate = dateFormat.format(currentDate); 
		System.out.println(date1);
		
		int currentHour = Integer.valueOf(todayDate.substring(11, 13));
		int scheduleHour = Integer.valueOf(date1.substring(11,13));
		
		int currentMinute = Integer.valueOf(todayDate.substring(14, 16));
		int scheduleMinute = Integer.valueOf(date1.substring(14, 16));
		
		for(int i=0;i<5;i++)
			driver.findElement(By.xpath("(//*[@data-action = 'decrementHours'])")).click();	
					
		if(scheduleMinute>29)
		{						
			for(int i=0;i<(currentMinute-scheduleMinute);i++)
			{
				driver.findElement(By.xpath("(//*[@data-action = 'decrementMinutes'])")).click();
				Thread.sleep(500);
			}
		}
	
		else
		{				
			for(int i = 0; i < 28 ; i++)
			{
				driver.findElement(By.xpath("(//*[@data-action = 'decrementMinutes'])")).click();
				Thread.sleep(500);
			}
			
			
		}
		
		//driver.findElement(scheduleDateInputField).sendKeys(date1);//Will set a campaign time after 3 minutes.
				
	}
*/
	
	
	public void setScheduleDateAndTime() throws Exception
	
	{
		
		for(int i = 0; i < 28 ; i++)
		{
			driver.findElement(By.xpath("(//*[@data-action = 'decrementMinutes'])")).click();
			Thread.sleep(250);
		}
			
		for(int i = 0; i < 5 ; i++)
		{
			driver.findElement(By.xpath("(//*[@data-action = 'decrementHours'])")).click();
			Thread.sleep(250);
		}
		
			
	}
	
	
	
	public WebElement isElementPresnt(WebDriver driver,By locator,int time)

	{
	  
	WebElement ele = null;
	 
	for(int i=0;i<time;i++)
	{
	try{
	ele=driver.findElement(locator);

//	ele=driver.findElement(xpathlocator);
		
		break;
	}
	catch(Exception e)
	{
	try 
	{
	Thread.sleep(1000);
	} catch (InterruptedException e1) 
	{
	System.out.println("Waiting for element to appear on DOM");
	}
	}
	 
	 
	}
	return ele;
	 
	}
	
	// Methods for username , passwords , URLs
	
	public void getPreProdURL()
	{
		driver.get("https://pre-prod-102.betaout.com/");	
	}
	

	public void getStagingURL()
	{
		driver.get("https://stag.betaout.in/");	
	}
	
	public void getLiveURL()
	{
		driver.get("https://app.betaout.com/");	
	}
	
	public String getWoocommerceOneURL()
	{
		String URL = "http://woocommerceone.betaout.in/";
		driver.get(URL);
		return URL;
	}
	
	
	public String getWoocommerceURL()
	{
		
		String URL = "http://woocommerce.betaout.in/";
		driver.get(URL);
		return URL;
		
	}
	
	public void getYopmailURL()
	{
		driver.get("http://www.yopmail.com/en/");
		
	}
	
	public void getPrestaShopURL()
	{
		driver.get("http://prestashop1616.basera.com/");
		
	}
	
	
	public void getPreProdCartCampaignPage()
	{
		driver.get("https://pre-prod-102.betaout.com/carts/cart-campaign");
		
	}
	
	public String getWoocommercetestUsername()
	{
		return "Woocommercetest@y0v.in";
	}
	
	public String getWoocommercetestPassword()
	{
		return "Woocommercetest";
	}
	

	public String getBetaoutpushUsername()
	{
		return "betaoutpush.multiple@getamplify.com";
	}
	
	public String getBetaoutpushPassword()
	{
		return "123456";
	}

	
	
	public String getSimranUsername()
	{
		return "sjs22858@gmail.com";
	}
	
	public String getSimranPassword()
	{
		return "simranjeet";
	}
	
	public String getShivangiUsername()
	{
		return "shivangi+1@devrover.com";
	}
	
	public String getShivangiPassword()
	{
		return "11111111";
	}
	
	public String getNavCartUsername()
	{
		return "navcartrecovery@gmail.com";
	}
	
	public String getNavCartPassword()
	{
		return "cartrecovery";
	}
	
	
	
public Setup_class(WebDriver driver)

{
	this.driver = driver;	
}

								}
	
