package common_Classes;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Homepage {

	public WebDriver driver;
	
	By insights = By.xpath("//div[text() = 'Insights']");
	By cust_segments = By.xpath("//div[text() = 'Customer Segments']");
	By email_marketing = By.xpath("//div[text() = 'Email Marketing']");
	By mobile_push = By.xpath("//div[text() = 'Mobile Push']");
	By onsite = By.xpath("//div[text() = 'On-site Marketing']");
	By sms_marketing = By.xpath("//div[text() = 'SMS Marketing']");
	By cart_recovery = By.xpath("//div[text() = 'Cart Recovery']");
	By browser_push = By.xpath("//div[text() = 'Browser Push']");
	By contact_database = By.xpath("//div[text() = 'Contact Database']");
	By product_catalog = By.xpath("//div[text() = 'Product Catalog']");
	By ideas_docs = By.xpath("//div[text() = 'Ideas & Docs']");

//	By accounts = By.xpath("//span[@class = 'icon-angle-down pull-right visible-lg visible-md']");
	By accounts = By.xpath("//*[@class = 'dropdown-toggle ']");
	
//	By search = By.xpath("//img[@src = '/parmanu2/images/sidebar-icons/Search.png']");
	By search = By.xpath("(//*[@title = 'Search for users....'])[2]");
	
//	By settings = By.xpath("//*[text() = 'Settings']");	
	By settings = By.xpath("(//*[@href = '/configurations/settings/team'])[2]");
	
//	By logout = By.xpath("//*[text() = 'logout']");
	By logout = By.xpath("(//*[text() = 'Logout'])[2]");		
	
	
	public Homepage(WebDriver driver)

	{
		this.driver = driver;

	}

	public void insight_click()

	{

		driver.findElement(insights).click();

	}

	public void cust_segments_click()

	{

		driver.findElement(cust_segments).click();

	}

	public void email_mktg_click()

	{

		driver.findElement(email_marketing).click();

	}

	public void mobilepush_click()

	{

		driver.findElement(mobile_push).click();

	}

	public void onsite_click()

	{

		driver.findElement(onsite).click();

	}

	public void sms_mktg_click()

	{

		driver.findElement(sms_marketing).click();

	}

	public void cartrecovery_click()

	{

		driver.findElement(cart_recovery).click();

	}

	public void browserpush_click()

	{

		driver.findElement(browser_push).click();

	}

	public void contact_db_click()

	{

		driver.findElement(contact_database).click();

	}

	public void product_catalog_click()

	{

		driver.findElement(product_catalog).click();

	}

	public void ideas_docs_click()

	{

		driver.findElement(ideas_docs).click();

	}

	public void accnt_click()

	{
		driver.findElement(accounts).click();

	}

	
	public void search_click()

	{
		driver.findElement(search).click();

	}
	
	
	public void settings_click()
	
	{

	/*List<org.openqa.selenium.WebElement> set = driver.findElements(settings);
    System.out.println("Total elements :"+set.size());
              
        ((org.openqa.selenium.WebElement) set.get(1)).click();*/
        
		driver.findElement(settings).click();
		
	}
	
	public void logout_click()
	
	{

	List<org.openqa.selenium.WebElement> log = driver.findElements(logout);
    System.out.println("Total elements :"+log.size());
              
        ((org.openqa.selenium.WebElement) log.get(1)).click();
        
	}
		
    
	
	
	
	
	
	
	
	
	
}
