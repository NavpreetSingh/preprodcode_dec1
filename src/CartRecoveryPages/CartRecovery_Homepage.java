package CartRecoveryPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartRecovery_Homepage {

	public WebDriver driver;
	
	
	public By Overview = By.xpath("(//*[@href = '/carts/cart'])[3]");
	public By Campaigns = By.xpath("(//*[@href = '/carts/cart-campaign'])[2]");
	public By Overview_Carts = By.xpath("(//*[@ty = 'cart'])");
	public By Overview_Revenue = By.xpath("(//*[@ty = 'gmv'])");
	public By Campaigns_CreateCampaign = By.xpath("(//*[@href = '/carts/create-new/'])[2]");
	public By Campaigns_NameofAllCampaigns = By.xpath("(//*[@style = ' display: inline-block;'])");        // By using index for iterating
	public By Campaigns_Edit = By.xpath("(//*[@title = 'Edit'])[1]");
	public By Campaigns_ActiveInstance = By.xpath("(//*[@title = 'Make Active'])[1]");
	public By Campaigns_InactiveInstance = By.xpath("(//*[@title = 'Make Inactive'])[1]");
	public By Campaigns_DeleteInstance = 	By.xpath("(//*[@title = 'Delete'])[2]");
	public By Campaigns_DeleteInstance_EnterDelete = By.xpath("(//*[@id = 'capsVal'])");
	public By Campaigns_DeleteInstance_ConfirmDeletebutton = By.xpath("(//*[text() = 'DELETE'])");	
	public By Campaigns_Clone = By.xpath("(//*[@title = 'Copy'])[1]");
	public By Campaigns_Clone_Confirm = By.xpath("(//*[@name = 'clone'])");
	
	
	public By Campaigns_total_sequences = By.xpath("//*[contains(@id,'project_email')]");
	public By Campaigns_total_subjects = By.xpath("(//*[contains(@id,'project_email')])/td");
	public By Campaigns_totalcampaigns = By.xpath("(//*[@class = 'camp-name'])");
	public By Campaigns_campaign_name = By.xpath("(//*[@class = 'camp-name'])[1]");
	public By Campaigns_campaign_createdinfo = By.xpath("(//*[@class = 'hp-main-figure'])[1]");
	
	
	public By WoocommerceAccount_MyAccount = By.xpath("(//*[text() = 'My Account'])");
	public By WoocommerceAccount_Username = By.xpath("(//*[@id = 'username'])");
	public By WoocommerceAccount_Password = By.xpath("(//*[@id = 'password'])");
	public By WoocommerceAccount_Login = By.xpath("(//*[@name = 'login'])");
	public By WoocommerceAccount_Home = By.xpath("(//*[text() = 'Home'])");
	public By WoocommerceAccount_AddToCart = By.xpath("(//*[@data-product_id = '697'])");
	public By WoocommerceAccount_ViewCart = By.xpath("(//*[@title = 'View Cart'])");

	
	public By YopmailAccount_Username = By.xpath("(//*[@id = 'login'])");
	public By YopmailAccount_CheckInbox= By.xpath("(//*[@title = 'Check inbox @yopmail.com'])");
	public By YopmailAccount_Iframe = By.id("ifinbox");
	public By YopmailAccount_TotalEmails = By.xpath("(//*[@class = 'lms'])");
	public By YopmailAccount_IframeText = By.id("ifmail");
	public By YopmailAccount_Text = By.cssSelector("p"); 

	
	public CartRecovery_Homepage(WebDriver driver)
	{
		this.driver = driver;
	}

	
	public void OverviewClick()
	{
		driver.findElement(Overview).click();
	}
	
	public void CampaignsClick()
	{
		driver.findElement(Campaigns).click();
	}
	
	public void Overview_CartsClick()
	{
		driver.findElement(Overview_Carts).click();
	}
	
	public void Overview_RevenueClick()
	{
		driver.findElement(Overview_Revenue).click();
	}
	
	public void Campaigns_CreateCampaignClick()
	{
		driver.findElement(Campaigns_CreateCampaign).click();
	}
	
	public void Campaigns_NameofAllCampaignsClick()
	{
		driver.findElement(Campaigns_NameofAllCampaigns).click();
	}
	
	
	
	public void Campaigns_EditClick()
	{
		driver.findElement(Campaigns_Edit).click();
	}
	
	
	
	public void Campaigns_ActiveInstanceClick()
	{
		driver.findElement(Campaigns_ActiveInstance).click();
	
	}
	
	
	public void Campaigns_InactiveInstanceClick()
	{
		driver.findElement(Campaigns_InactiveInstance).click();
	
	}
	
	
	public void Campaigns_DeleteInstanceClick() throws Exception
	{
		driver.findElement(Campaigns_DeleteInstance).click();
		driver.findElement(Campaigns_DeleteInstance_EnterDelete).sendKeys("DELETE");
		Thread.sleep(2000);
		driver.findElement(Campaigns_DeleteInstance_ConfirmDeletebutton).click();
		Thread.sleep(2000);
		
	}
	
	public void Campaigns_CloneClick() throws Exception
	{
		driver.findElement(Campaigns_Clone).click();
		Thread.sleep(2000);
		driver.findElement(Campaigns_Clone_Confirm).click();
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
