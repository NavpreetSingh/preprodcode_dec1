package CartRecoveryPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartRecovery_SelectTemplate {

	public WebDriver driver;
	
	By Preview_GarmentsTemplate = By.xpath("(//*[@id = '339'])");
	By UseTemplate_EdibleTemplate = By.xpath("(//*[@data-tid = '332'])");
	
	
	public CartRecovery_SelectTemplate(WebDriver driver)
	{
		this.driver = driver;
	}
	
	
	public void Preview_GarmentsTemplateClick()
	{
		driver.findElement(Preview_GarmentsTemplate).click();
	}
	

	public void UseTemplate_GarmentsTemplateClick()
	{
		driver.findElement(UseTemplate_EdibleTemplate).click();
	}

	
}
