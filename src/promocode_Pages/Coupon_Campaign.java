package promocode_Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Coupon_Campaign {

	public WebDriver driver;

	By add_coupon_codes = By.xpath("//span[text() = 'ADD COUPON CODES']");
	
	By active_campaign = By.xpath("(//*[@class = 'resumeCouponCampign'])[1]");

	By resume_campaign = By.xpath("//*[@name = 'resumeCouponList']");
	
	By delete_campaign = By.xpath("(//*[@class = 'deleteEmailCampignType'])[5]");

	By enterdelete = By.xpath("//*[@id = 'capsVal']");

	By totalcodes = By.xpath("(//*[@class = 'text-center greyBg'])[1]");
	
	By distributedcodes = By.xpath("(//*[@class = 'text-center greyBg'])[2]");
	
	By unusedcodes = By.xpath("(//*[@class = 'text-center greyBg'])[3]");
	
	By dateofupload = By.xpath("(//*[@class = 'time'])[1]");
	
	By CouponName = By.xpath("(//*[@class = 'selectable draft'])/td[2]");    // Select by indexing
	
	
	By CampaignsAssociated = By.xpath("(//*[@class = 'selectable draft'])/td[3]");
	
	public Coupon_Campaign(WebDriver driver)

	{
		this.driver = driver;

	}

	public void add_coupon_codes_button()

	{

		driver.findElement(add_coupon_codes).click();

	}
	
	
	public void activecampaign_click() throws Exception
	
	{
		
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(active_campaign)).build().perform();	
		Thread.sleep(2000);
		driver.findElement(active_campaign).click();
		
	}
	
	public void resumecampaign_click()
	
	{
		driver.findElement(resume_campaign).click();
		
	}

	public void deletecampaign_click() throws InterruptedException

	{
		List<org.openqa.selenium.WebElement> delete = driver.findElements(delete_campaign);
	  //  System.out.println("Total elements :"+delete.size());
	              
	        ((org.openqa.selenium.WebElement) delete.get(5)).click();
	        
	        driver.findElement(enterdelete).sendKeys("DELETE");
	        Thread.sleep(2000);
	        driver.findElement(By.xpath("//*[@name = 'deleteCouponList']")).click();
	        
	}
	
	
	public void totalcodesprint()
	{
		
	String total = 	driver.findElement(totalcodes).getText();
	System.out.println("Total codes are = "+total);	
	
	}
	
	
	
	public void distributedcodesprint()
	{
		
	String distributed = 	driver.findElement(distributedcodes).getText();
	System.out.println("Distributed codes are = "+distributed);	
	
	}
	
	
	public void unusedcodesprint()
	{
		
	String unused = 	driver.findElement(unusedcodes).getText();
	System.out.println("Unused codes are = "+unused);	
	
	}
	
	
	public void dateofupload()
	{
		
		String date = driver.findElement(dateofupload).getText();
		System.out.println("Date of Upload = "+date);
	}
	
	
	public void totalCampaigns()
	{
	
	List<WebElement> allCampaigns	=	driver.findElements(CouponName);
	System.out.println("Total number of campaigns are = "+allCampaigns.size());	
	
	/*String campaignName = allCampaigns.get(0).getText();
	System.out.println("Name of the campaign = "+campaignName);*/
	
	}
	
	
	
	
	public void CampaignsAssociatedPrint()
	{
	
	List<WebElement> allAssociatedCampaigns	=	driver.findElements(CampaignsAssociated);
	
	String associatedNumber = allAssociatedCampaigns.get(0).getText();
	System.out.println("Number of associated campaigns = "+associatedNumber);
	
	}
	
	
}
